---
title: "About"
date: 2022-06-19T01:42:04-03:00
draft: false
---

I'm Iñaki Martin Rodriguez Reboredo, I'm an independent developer and software maintainer.

I contribute to Free and Open Source projects from time to time but always with a fistful of enthusiasm.
I use Arch Linux as my daily driver, where I do development and testing. I also maintain some packages in the AUR.

My interests are Programming, Graphics Design, Mathematics, Linguistics, AI, Multimedia Editing and Compositing.
My hobbies and pastimes are reading comics, programming, maintaining my system, watching series and going out for a jog.

Both my main GitLab and GitHub handles are **@YakoYakoYokuYoku** but I use **@matrodreb** for job connections and for hosting
this site.

{{< fontawesome gitlab "https://gitlab.com/YakoYakoYokuYoku" "@YakoYakoYokuYoku" >}}
{{< linebreak >}}
{{< fontawesome github "https://github.com/YakoYakoYokuYoku" "@YakoYakoYokuYoku" >}}
{{< linebreak >}}
{{< linebreak >}}
{{< fontawesome gitlab "https://gitlab.com/martrodreb" "@martrodreb" >}}
{{< linebreak >}}
{{< fontawesome github "https://github.com/martrodreb" "@martrodreb" >}}
