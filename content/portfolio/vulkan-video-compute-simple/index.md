---
title: Vulkan compute video demo
description: An example program that applies shaders to videos
date: "2021-11-23T16:57:03-03:00"
jobDate: August 2021
work: [programming, video]
techs: [c, vulkan, ffmpeg]
designs: [Graphviz]
thumbnail: images/demo-sobel.png
projectUrl: https://gitlab.com/YakoYakoYokuYoku/videocomputedemo
---

An example of the usage of Vulkan for video processing.

In this demo program a video pipeline is employed to successfully process input video to filtered output. This consists in the
demuxing and decoding of input, the upload of the decoded frames to the Vulkan pipeline, the filter application, the download
of the resulting textures and the encoding and muxing of the processed frames.
