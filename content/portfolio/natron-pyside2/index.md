---
title: Natron PySide2 port
description: Ported the Natron compositor to Qt5/PySide2
date: "2021-11-23T16:06:43-03:00"
jobDate: November 2021
work: [programming, video]
techs: [c++, python, qt]
designs: [GIMP]
thumbnail: images/natron-pyside2-port.png
projectUrl: https://natrongithub.github.io/
---

Due to the deprecation of Qt4/PySide, Natron was lagging behind a little with respect to having a more up to date Qt graphical user interface experience.

Thanks to the previous works of the [Python3 port](https://github.com/NatronGitHub/Natron/pull/686) this new effort to port it to Qt5/PySide2 is now possible and [is now in the works](https://github.com/NatronGitHub/Natron/pull/698) aiming to impulse Natron in the digital compositing field.

The realization of the work has implicated a good amount of knowledge in both build systems and programming.

I'm looking forward in the future to work on modernizing bits of the codebase and to make the Qt6/PySide6 port too, apart from more exciting releases.
