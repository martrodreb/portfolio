---
title: Vulkan SPIRV GStreamer filter
description: GStreamer video filtering via Vulkan SPIRV shaders
date: "2022-02-19T16:53:09-03:00"
jobDate: February 2022
work: [programming, video]
techs: [c, vulkan, gstreamer]
designs: [Graphviz]
thumbnail: images/wavy-testsrc.png
projectUrl: https://gitlab.freedesktop.org/gstreamer/gstreamer/-/merge_requests/1197
---

Vulkan video processing was introduced a while ago in the GStreamer bad plugins collection. Featuring many things like
transfering frames to the GPU, video display, color and view conversion, etc. But I've found that it was missing something
that the OpenGL filters had, and that was shader filtering.

Things were fortunately not hard due to my previous work on the compute demo and modeling on existing filters within the
source code. The result was `vulkanshaderspv`, named after that it's a Vulkan video filter that uses SPIRV shaders (as it's
the standard shader format for Vulkan).

The applications of this are multiple, like doing sobel filters for computer vision, quick blurring or sharpening and other
high performant video effects.
